# MLSQL Lang example project

This is a mlsql-lang example project.

## Installation Of MLSQL Lang IDE


1. Download [Visual Studio Code](https://kyligence.feishu.cn/docs/doccnVlsVfEFsD9YQYeSm9i1Hty#)
2. Download MLSQL Lang VSCode plugin
    * [Winwdows](http://download.mlsql.tech/mlsql-win-0.0.6.vsix)
    * [Mac](http://download.mlsql.tech/mlsql-mac-0.0.6.vsix)
    * [Linux](http://download.mlsql.tech/mlsql-linux-0.0.6.vsix)



After installation of `Visual Studio Code` , switch to  `Extensions` tab, click `...` on right side of search bar, find the item `Install from VSIX...`, choose the VSCode plugin we had already downloaded in preview step and intall it.

![](http://store.mlsql.tech/upload_images/fcc2091a-db9a-4248-96d9-680bc32a7594.png)


Notice that for now we only support Ligt theme in vscode.
Select Code > Preferences > Color Theme :

![](http://store.mlsql.tech/upload_images/011d67b6-0a98-445f-9e59-8c940462718e.png)


In command palette popup，select the light color:

![](http://store.mlsql.tech/upload_images/96b0e81f-1856-4c8a-9bb6-84d8180e7968.png)

## Download this project

Click `Clone or download` and select  `Download ZIP`, then you get the package of this project. Unzip it in you desktop.

![](http://store.mlsql.tech/upload_images/f67b7e1d-968d-4a2f-af36-3c0e14730d83.png)


## Open this project in vscode

Select File > Open...  and choose the location where we unzip this project.

## Tutorial

1. `./src/try_mlsql` is a good start point for you to learn MLSQL.
2. `./src/a_tour_of_mlsql` you can learn full picture of MLSQL Lang.


## ./src/try_mlsql

![./src/try_mlsql_1.png](./src/images/try_mlsql_1.png)
![./src/try_mlsql_2.png](./src/images/try_mlsql_2.png)


## ./src/a_tour_of_mlsql

![./src/tour_1.png](./src/images/tour_1.png)
![./src/tour_2.png](./src/images/tour_2.png)


